## v2.0.0 (2017/03/16)
* Migratrion from pycontrol-gui project.

## v.1.4.0 (2017/02/17)
* Compliance with pyforms v1.0.beta
* Updates dependencies versions

## v.1.3.1 (2016/12/02)
Implemented changes according to: https://bitbucket.org/fchampalimaud/pycontrol-gui/wiki/Proposed%20GUI%20enhancements
* Modify data file format (what is missing is include the pyboards hardware unique ID and save the name of the hardware definition)
* On the GUI Setup was replaced by Subject (the folder structure was not changed yet because it requires more changes and it is more error-prone)

## v.1.3.0 (1.3) (2016/11/22)
Merge with current framework.
