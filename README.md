# PyBehavior

**PyBehavior** is a *Graphical User Interface (GUI)* library that allows rapid development of behavior software. It is written in Python3 and built on top of [PyForms](https://github.com/UmSenhorQualquer/pyforms). Currently, it supports both PyControl and BPod behavior software.

![pyControlGUI frontpage](https://bytebucket.org/fchampalimaud/pycontrol-gui/wiki/media/PyBehavior_layers.png?rev=bae0cf0ae21f0f7cffade1f2e76645e84530d5a6)

Please see [ReadTheDocs](http://pybehavior.readthedocs.io/en/latest/) for more information.

## Distribution

**PyBehavior** is distributed as a standalone executable for Mac and Windows:

* [Windows installer](https://bitbucket.org/fchampalimaud/pycontrol-gui/downloads)
* [Mac installer](https://bitbucket.org/fchampalimaud/pycontrol-gui/downloads)


## Development
First, install [PyForms](https://github.com/UmSenhorQualquer/pyforms):

    pip install https://github.com/UmSenhorQualquer/pyforms/archive/master.zip

Finally, run it:

	python3 -m pybehavior
	
## Setting environment

PyBehavior relies on [PyForms](https://github.com/UmSenhorQualquer/pyforms) which relies on PyQt4. Please refer to the PyForms documentation on how to set the environment.