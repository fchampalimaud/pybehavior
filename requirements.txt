# DO NOT CHANGE THIS FILE
# run like this: pip3 install -r requirements.txt --upgrade

# external libraries
Send2Trash>=1.3.0
https://bitbucket.org/fchampalimaud/logging-bootstrap/get/master.zip
https://github.com/UmSenhorQualquer/pysettings/archive/master.zip
https://github.com/UmSenhorQualquer/pyforms/archive/v1.0.beta.zip
https://bitbucket.org/fchampalimaud/pyforms-generic-editor/get/master.zip
