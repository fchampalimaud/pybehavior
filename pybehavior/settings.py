# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging, os

SETTINGS_PRIORITY = 10

APP_LOG_FILENAME = 'pybehavior.log'
APP_LOG_HANDLER_FILE_LEVEL = logging.DEBUG
APP_LOG_HANDLER_CONSOLE_LEVEL = logging.INFO

PYFORMS_LOG_HANDLER_FILE_LEVEL = logging.DEBUG
PYFORMS_LOG_HANDLER_CONSOLE_LEVEL = logging.INFO

CONTROL_EVENTS_GRAPH_DEFAULT_SCALE = 100

BOARD_LOG_WINDOW_REFRESH_RATE = 1000

PYBEHAVIOR_LOAD_PROJECT = None

GENERIC_EDITOR_WINDOW_GEOMETRY = 100, 100, 800, 600
GENERIC_EDITOR_MODEL = 'pybehavior.models.projects.Projects'
GENERIC_EDITOR_TITLE = 'PyBehavior GUI'
GENERIC_EDITOR_PLUGINS_PATH = None
GENERIC_EDITOR_PLUGINS_LIST = []

PYFORMS_STYLESHEET = os.path.join('pybehavior', 'resources', 'themes', 'default', 'stylesheet.css')
PYFORMS_STYLESHEET_DARWIN = os.path.join('pybehavior', 'resources', 'themes', 'default', 'stylesheet_darwin.css')
