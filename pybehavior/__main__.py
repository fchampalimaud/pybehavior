# !/usr/bin/python
# -*- coding: utf-8 -*-

import loggingbootstrap
import logging

try:
	from pysettings import conf

	# Initiating logging for pysettings. It has to be initiated manually here because we don't know yet
	# the logger filename as specified on settings
	loggingbootstrap.create_double_logger("pysettings", logging.INFO, 'pybehavior.log', logging.INFO)

except ImportError as err:
	logging.getLogger().critical(str(err), exc_info=True)
	exit("Could not load pysettings! Is it installed?")

try:
	import pyforms_generic_editor
except ImportError as err:
	logging.getLogger().critical(str(err), exc_info=True)
	exit("Could not load pyforms_generic_editor! Is it installed?")

try:
	import pyforms
except ImportError as err:
	logging.getLogger().critical(str(err), exc_info=True)
	exit("Could not load pyforms! Is it installed?")

# https://docs.python.org/3.5/library/multiprocessing.html#multiprocessing.freeze_support
#from multiprocessing import freeze_support  # @UnresolvedImport

from pybehavior.editor import Editor

#freeze_support()

# setup different loggers but output to single file
loggingbootstrap.create_double_logger("pybehavior", conf.APP_LOG_HANDLER_CONSOLE_LEVEL, conf.APP_LOG_FILENAME,
                                      conf.APP_LOG_HANDLER_FILE_LEVEL)
loggingbootstrap.create_double_logger("pyforms", conf.PYFORMS_LOG_HANDLER_CONSOLE_LEVEL, conf.APP_LOG_FILENAME,
                                      conf.PYFORMS_LOG_HANDLER_FILE_LEVEL)
loggingbootstrap.create_double_logger("pyforms_generic_editor", conf.APP_LOG_HANDLER_CONSOLE_LEVEL,
                                      conf.APP_LOG_FILENAME,
                                      conf.APP_LOG_HANDLER_FILE_LEVEL)

#logging.getLogger("pycontrolgui").debug("Using multiprocessing: %s", conf.USE_MULTIPROCESSING)


def start():
	pyforms.start_app(Editor, conf.GENERIC_EDITOR_WINDOW_GEOMETRY)


if __name__ == '__main__':
	start()
