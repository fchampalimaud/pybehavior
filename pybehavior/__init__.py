# !/usr/bin/python3
# -*- coding: utf-8 -*-

__version__ = "2.0.0.beta"
__author__ = "Carlos Mao de Ferro"
__credits__ = ["Carlos Mao de Ferro", "Ricardo Ribeiro"]
__license__ = "Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>"
__maintainer__ = ["Carlos Mao de Ferro", "Ricardo Ribeiro"]
__email__ = ["cajomferro@gmail.com", "ricardojvr@gmail.com"]
__status__ = "Development"

from pysettings import conf;

conf += 'pybehavior.settings'

# load the user settings in case the file exists
try:
	import user_settings

	conf += user_settings
except Exception as err:
	print('Error when importing user_settings!!!')
	print(str(err))
