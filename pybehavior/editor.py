# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pysettings import conf

from pyforms_generic_editor.editor.base_editor import BaseEditor


class Editor(BaseEditor):
	def __init__(self):
		global conf;
		conf += 'pybehavior.resources'  # Resources can only be loaded after pyqt is running

		super(Editor, self).__init__()

	def init_form(self):
		super(Editor, self).init_form()

		# FOR DEVELOPMENT: Load a project automatically.
		if conf.PYBEHAVIOR_LOAD_PROJECT:
			proj = self.model.create_project()
			proj.load(conf.PYCONTROLGUI_LOAD_PROJECT)

		# proj.export_code()
