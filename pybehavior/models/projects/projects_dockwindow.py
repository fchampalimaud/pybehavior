# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pysettings import conf

from pybehavior.models.projects.projects_treenode import ProjectsTreeNode


class ProjectsDockWindow(ProjectsTreeNode):
	def __init__(self, mainwindow=None):
		ProjectsTreeNode.__init__(self)

		self.mainwindow = mainwindow
		self.mainwindow.dock.value = self

		self.register_on_main_menu(self.mainwindow.mainmenu)

	def register_on_main_menu(self, mainmenu):
		filemenu = mainmenu[0]
		filemenu['File'].insert(0, {'New project': self.create_project, 'icon': conf.NEW_SMALL_ICON})
		filemenu['File'].insert(1, '-')
		filemenu['File'].insert(2, {'Open a project': self.open_project, 'icon': conf.OPEN_SMALL_ICON})
		filemenu['File'].insert(3, '-')
		filemenu['File'].insert(4,
		                        {'Save current project': self.save_current_project, 'icon': conf.SAVE_SMALL_ICON})
		filemenu['File'].insert(5, {'Duplicate current project': self.save_current_project_as,
		                            'icon': conf.SAVE_SMALL_ICON})
		filemenu['File'].insert(6, {'Save all projects': self.save_all_projects, 'icon': conf.SAVE_SMALL_ICON})
		filemenu['File'].insert(7, '-')
