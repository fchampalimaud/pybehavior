# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging

logger = logging.getLogger(__name__)

from pybehavior.models.project.project_treenode import ProjectTreeNode


class ProjectDockWindow(ProjectTreeNode):
	def show(self): self.mainwindow.details.value = self

	@property
	def mainwindow(self): return self.projects.mainwindow

	def close(self):
		self.mainwindow.details.value = None
		super(ProjectDockWindow, self).close()
