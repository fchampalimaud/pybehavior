# !/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt4 import QtGui

import pyforms
from pyforms import BaseWidget
from pyforms.Controls import ControlTree

from pybehavior.models.project import Project
from pybehavior import utils


class ProjectsWindow(BaseWidget):
	def __init__(self):
		super(ProjectsWindow, self).__init__('Projects')

		self._projects = []
		self.tree = ControlTree('Projects')
		self.tree.item_selection_changed = self._tree_item_sel_changed
		self.clear()

	def _tree_item_sel_changed(self):
		"""
		Called when a different item in the tree is selected. In that case,
		show the detail's window of the corresponding item.
		If the item has no corresponding window, show an empty container.
		"""

		selected = self.tree.selected_item
		if hasattr(selected, 'window'): selected.window.show()

	def __add__(self, obj):
		self._projects.append(obj)
		return self

	def __sub__(self, obj):
		if isinstance(obj, Project): self._projects.remove(obj)
		return self

	def clear(self):
		self.tree.setDragEnabled(False)
		self.tree.setHeaderLabel('Please open or create project')
		self.tree.show_header = True

	def create_project(self):
		return Project(self)

	def open_project(self, project_path=None):
		self.tree.item_selection_changed_event = utils.do_nothing

		if not project_path:
			project_path = QtGui.QFileDialog.getExistingDirectory(self, "Select the project directory")
		if project_path:
			proj = self.create_project()
			proj.load(str(project_path))

		self.tree.item_selection_changed_event = self._tree_item_sel_changed

	def save_all_projects(self):
		for project in self.projects: project.save()

	def save_current_project(self):
		selected = self.tree.selected_item
		if hasattr(selected, 'window'):
			if isinstance(selected.window, Project):
				selected.window.save()
			else:
				selected.window.project.save()

	def save_current_project_as(self):
		selected = self.tree.selected_item
		if hasattr(selected, 'window'):
			if isinstance(selected.window, Project):
				selected.window.save_as()
			else:
				selected.window.project.save_as()

	@property
	def projects(self):
		return self._projects


if __name__ == "__main__": pyforms.start_app(ProjectsWindow)
