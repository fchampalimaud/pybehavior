# !/usr/bin/python3
# -*- coding: utf-8 -*-


from pyforms.Controls import ControlTree

from pybehavior.models.projects.projects_window import ProjectsWindow


class ProjectsTreeNode(ProjectsWindow):
	def __init__(self):
		ProjectsWindow.__init__(self)

		self.tree = ControlTree('Projects')

		self.tree.item_selection_changed_event = self.__item_sel_changed_evt
		self.clear()

	def __item_sel_changed_evt(self):
		selected = self.tree.selected_item
		if hasattr(selected, 'window'): selected.window.show()

	def clear(self):
		self.tree.setDragEnabled(False)
		self.tree.show_header = False
		self.tree.clear()
