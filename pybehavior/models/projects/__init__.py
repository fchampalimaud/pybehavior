# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pysettings import conf

from pybehavior.models.projects.projects_signals import ProjectsSignals

Projects = type(
	'Projects',
	tuple(conf.GENERIC_EDITOR_PACKAGES_FINDER.find_class('models.projects.Projects') + [ProjectsSignals]),
	{}
)
