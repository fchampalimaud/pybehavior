# !/usr/bin/python3
# -*- coding: utf-8 -*-

from pysettings import conf

from pybehavior.models.project.project_uibusy import ProjectUIBusy

Project = type(
	'Project',
	tuple(conf.GENERIC_EDITOR_PACKAGES_FINDER.find_class('models.project.Project') + [ProjectUIBusy]),
	{}
)
