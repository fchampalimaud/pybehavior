# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import os

from PyQt4 import QtGui

import pyforms
from pyforms import BaseWidget
from pyforms.Controls import ControlText
from pyforms.Controls import ControlButton

from pybehavior.models.project.project_io import ProjectIO as Project

logger = logging.getLogger(__name__)


class ProjectWindow(Project, BaseWidget):
	""" ProjectWindow represents the project entity as a GUI window"""

	def __init__(self):
		BaseWidget.__init__(self, 'Project')
		self.layout().setContentsMargins(5, 10, 5, 5)

		self._name = ControlText('Project name')
		self._path = ControlText('Project path')
		self._install_framework_btn = ControlButton('Install the framework on all boards')

		self.formset = ['_name', '_path', '_install_framework_btn', ' ']

		self._name.changed_event = self.__name_edited_evt
		self._path.value = self.path
		self._path.enabled = False
		# self._install_framework_btn.value = self.__install_framework_evt

		Project.__init__(self)

	def __name_edited_evt(self):
		if not hasattr(self, '_update_name') or not self._update_name:
			self.name = self._name.value

	# def __install_framework_evt(self):
	# 	for board in self.boards:
	# 		if board.status > Board.STATUS_READY:
	# 			QtGui.QMessageBox.about(self, "Board busy",
	# 			                        "The board [{0}] is busy. This action is only possible when all the boards are free.".format(
	# 				                        str(board)))
	# 			return
	#
	# 	for board in self.boards: board.install_framework()

	@property
	def path(self):
		return self._path.value

	@path.setter
	def path(self, value):
		self._path.value = value

	@property
	def name(self):
		return self._name.value

	@name.setter
	def name(self, value):
		self._update_name = True  # Flag to avoid recurse calls when editing the name text field
		self._name.value = value
		self._update_name = False

	def save(self, project_path=None):
		if project_path:
			Project.save(self, project_path)
		elif self.path:
			Project.save(self, self.path)
		else:
			folder = QtGui.QFileDialog.getExistingDirectory(self, "Select a directory to save the project: {0}".format(
				self.name))
			if folder:
				folder = os.path.join(folder, self.name)
				try:
					Project.save(self, str(folder))
				except FileExistsError as err:
					logger.warning(str(err))
					QtGui.QMessageBox.warning(self, 'Project exists',
					                          'Project with same name already exists. Please select another path.')

	def close(self):
		self.projects -= self
		super(ProjectWindow, self).close()


# Execute the application
if __name__ == "__main__":
	pyforms.start_app(ProjectWindow)
