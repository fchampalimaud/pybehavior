# !/usr/bin/python3
# -*- coding: utf-8 -*-

import os

from PyQt4 import QtGui


def path(filename): 
	return os.path.join(os.path.dirname(__file__), 'icons', filename)


ADD_SMALL_ICON = QtGui.QIcon(path('add.png'))
NEW_SMALL_ICON = QtGui.QIcon(path('new.png'))
OPEN_SMALL_ICON = QtGui.QIcon(path('open.png'))
SAVE_SMALL_ICON = QtGui.QIcon(path('save.png'))
EXIT_SMALL_ICON = QtGui.QIcon(path('exit.png'))
CLOSE_SMALL_ICON = QtGui.QIcon(path('close.png'))

PLAY_SMALL_ICON = QtGui.QIcon(path('play.png'))
BUSY_SMALL_ICON = QtGui.QIcon(path('busy.png'))
PROJECT_SMALL_ICON = QtGui.QIcon(path('project.png'))
REMOVE_SMALL_ICON = QtGui.QIcon(path('remove.png'))
